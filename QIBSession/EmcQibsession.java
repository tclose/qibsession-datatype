/*
 * GENERATED FILE
 * Created on Thu Apr 21 17:42:46 CEST 2016
 *
 */
 
/*
 * Copyright 2016 Biomedical Imaging Group Rotterdam, Departments of
 * Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
 
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

import java.net.*;
import java.io.*;
import org.json.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class EmcQibsession extends BaseEmcQibsession {

	public EmcQibsession(ItemI item)
	{
		super(item);
	}

	public EmcQibsession(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseEmcQibsession(UserI user)
	 **/
	public EmcQibsession()
	{}

        private boolean validNode = false;
        private String ontologyLabel = "";
        private String ontologyDescription = "";

        public boolean validNode()
        {
            return this.validNode;
        }

        public String ontologyLabel() {
            return this.ontologyLabel;
        }

        public String ontologyDescription() {
            return this.ontologyDescription;
        }

        protected boolean validOntologyNode(JSONObject ontology_json)
        {
            boolean valid = false;

            try {
                valid = 
                    ontology_json.has("_embedded") &&
                    ontology_json.getJSONObject("_embedded").has("terms") &&
                    ontology_json.getJSONObject("_embedded").getJSONArray("terms").length() > 0 &&
                    ((JSONObject)ontology_json.getJSONObject("_embedded").getJSONArray("terms").get(0)).has("description") &&
                    ((JSONObject)ontology_json.getJSONObject("_embedded").getJSONArray("terms").get(0)).getString("description").length() > 0;
            }
            catch(JSONException ex)
            {
                valid = false;
            }
            return valid;
        }

        public String ToEmcString(String ontology, String IRI, String td_label_id, String td_description_id, String counter)
        {
          String result = "";

          try
          {
              URL oracle = new URL("http://www.ebi.ac.uk/ols/api/ontologies/" + ontology.toUpperCase() + "/terms?obo_id=" + ontology.toUpperCase() + ":" + IRI);
              BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));
              
              String inputLine;
	      String ontologyJSON = "";
              while ((inputLine = in.readLine()) != null)
              {
                  ontologyJSON += inputLine;
              }
              in.close();

              JSONObject jsonObject = new JSONObject(ontologyJSON);
              this.validNode = validOntologyNode(jsonObject);
              if (this.validNode) {
                JSONObject terms = (JSONObject) jsonObject.getJSONObject("_embedded").getJSONArray("terms").get(0);
                this.ontologyLabel = terms.getString("label");
                this.ontologyDescription = terms.getJSONArray("description").getString(0);
              }
          }
          catch(FileNotFoundException ex)
          {
              result += ex.getMessage();
          }
          catch(MalformedURLException ex)
          {
              result += ex.getMessage();
          }
          catch(IOException ex)
          {
              result += ex.getMessage();
          }
          catch(JSONException ex)
          {
              result += ex.getMessage();
          }

          return result;
        }

	public EmcQibsession(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
