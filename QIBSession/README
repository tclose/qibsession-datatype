The Quantitative Imaging Biomarker (QIB) session is an XNAT experiment datatype to store a variable name, value, unit, ontologyname and ontology IRI. 
Furthermore, it can contain descriptions of a related paper, the session itself and link to associated sessions and resources.

The name and description of the ontology node are automatically retrieved via REST from EBI OLS.
Links are created to the EBI OLS description and a visual ontology browser OLSVis.

Steps to install the datatype:
 - Follow the installation instructions at https://wiki.xnat.org/display/XNAT16/Customizing+Data+Types
  (used version Jul 31, 2012 for this README, there is no Step 1) while overriding Step 2, sub-step 1 with:
   - place the file XDATScreen_report_EMC_QIBSession.vm in [XNAT]/projects/[webapp]/src/templates/screens/
   - place the file XDATScreen_report_EMC_QIBSession_overview.vm in [XNAT]/projects/[webapp]/src/templates/screens/
   - place the file EmcQibsession.java in [XNAT]/projects/[webapp]/src/java/org/nrg/xdat/om/
   - place the directory qibsession in [XNAT]/projects/[webapp]/src/schemas/
   - place the file ontologyLoader.js in [XNAT]/projects/[webapp]/src/scripts/
  - For Step 4: If unix-user based authentication is used for Postgresql, you can use:
   - sudo -u xnat psql -d xnat -f [webapp]-update.sql
  - For Step 6: After selecting the datatype from the drop down, all settings can be left to default (except if preferred otherwise), except:
   - On the second screen, set Singular name to `QIB Session' and the Plural Name to `QIB Sessions', without quotes.

Notes for the above installation instructions:
 - Placing source in projects, overrides default or auto-generated generated source code.
 - Placing screens in templates, overrides auto-generated source in base-templates.

Important:
 - Datatype schema's cannot be easily changed after installation in the database!
   Although the tables can be removed of the database and the tables regenerated with the sql scripts for small changes, this will also remove all stored data, making previously stored sessions corrupt.
   However, we also had sometimes problems (so far) with this approach, mostly for larger changes, and as a result, had to recreate the XNAT database from time to time.
   We plan to find out which tables and relations are created and linked, so the changes can perhaps be more easily undone or updated.
 - The source code is a prototype and currently not meant for production server use.

For more information about storing derived data and resources, also see: https://wiki.xnat.org/display/XNAT16/Strategies+for+XNAT+Image+Data+Storage

To create an QIB Session, the qibsession.xml can be used as an example. 
Note that the base sessions on which the session is derived should link to an existing session(s) to see it properly on the webpage.
A new QIB Session instance be send to XNAT with the following link:
 - curl -X PUT -u [USERNAME]:[PASSWORD] -T [PATH_TO_XML]" [XNAT IP/URL]/data/projects/[PROJECT]/subjects/[SUBJECT]/experiments/[EXPERIMENT]?inbody=true

To send a resource with pyxnat (https://pythonhosted.org/pyxnat/), you can use, after going to the session:
 - theSession.resource('[DIRECTORY]').file('[REMOTE FILENAME]').insert('[LOCAL PATH]', format="[EXTENSION]")
