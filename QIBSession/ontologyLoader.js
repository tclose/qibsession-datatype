/*
 * Copyright 2016 Biomedical Imaging Group Rotterdam, Departments of
 * Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

function validOntologyNode(ontology_json) {
    return ontology_json.hasOwnProperty('_embedded') &&
           ontology_json._embedded.hasOwnProperty('terms') &&
           ontology_json._embedded.terms.length > 0 &&
           ontology_json._embedded.terms[0].hasOwnProperty('description') &&
           ontology_json._embedded.terms[0].description.length > 0;
}

function hideDescription(td_description_id, counter) {
    var description = $("#"+td_description_id+"full_"+counter).html();
    var html = "";
  
    html += "<span style=\display:block;overflow:auto;max-width:240px;white-space:normal;\">";
    html += description.substring(0, 110);
    html += "<span style=\display:inline;white-space:nowrap;\">";
    html += (description.length >= 110 ? "&hellip;<a href=\"#\" style=\"cursor:hand;display:inline;\" onClick=\"showDescription('ontology_description_', " + counter + ");return false;\">[More]</a>" : "");
    html += "</span>";
    html += "</a>";

    $("#"+td_description_id+counter).html(html);
}

function showDescription(td_description_id, counter) {
    $("#"+td_description_id+counter).html(
        "<span style=\"display:block;white-space:normal;overflow:auto;max-width:200px;\">" +
        $("#"+td_description_id+"full_"+counter).html() +
        " <a href=\"#\" onClick=\"hideDescription('" + td_description_id + "'," + counter + ");return false;\">[Collapse]</a>"
    );
}

function loadOntologyNode(ontology, IRI, td_label_id, td_description_id, counter) {
    var label = "<i>Label could not be found</i>";
    var description = "<i>Description could not be found</i>";

    $.getJSON(
        "http://www.ebi.ac.uk/ols/beta/api/ontologies/" + ontology.toUpperCase() + "/terms?obo_id=" + ontology.toUpperCase() + ":" + IRI,
       function(ontology_json) {
            if (validOntologyNode(ontology_json)) {
                label = ontology_json._embedded.terms[0].label;
                description = ontology_json._embedded.terms[0].description[0];
            }
            $("#"+td_label_id+counter).html("<span style=\"display:block;max-width:100px;overflow:auto;white-space:normal;\">" + label + "</span>");
            $("#"+td_description_id+counter).html(
                "<a href=\"#\" style=\"cursor:hand\" onClick=\"showDescription('" + td_description_id + "'," + counter + ");return false;\"><u>" +
                description.substring(0, 80) +
                (description.length >= 80 ? " [...]" : "") + 
                "</u></a>");
            $("#"+td_description_id+"_full"+counter).html(description);
            $("#"+td_description_id+counter).attr("fulltext", description);
        }
    );
}
