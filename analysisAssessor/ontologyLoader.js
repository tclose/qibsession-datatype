function validOntologyNode(ontology_json) {
    return ontology_json.hasOwnProperty('_embedded') &&
           ontology_json._embedded.hasOwnProperty('terms') &&
           ontology_json._embedded.terms.length > 0 &&
           ontology_json._embedded.terms[0].hasOwnProperty('description') &&
           ontology_json._embedded.terms[0].description.length > 0;
}

function hideDescription(td_description_id, counter) {
    var description = $("#"+td_description_id+counter).attr("fulltext");
    $("#"+td_description_id+counter).html(
        "<a href=\"#\" style=\"cursor:hand\" onClick=\"showDescription('" + td_description_id + "'," + counter + ")\"><u>" +
        description.substring(0, 30) +
        (description.length >= 30 ? " [...]" : "") +
        "</u></a>");
}

function showDescription(td_description_id, counter) {
    $("#"+td_description_id+counter).html(
        "<span style=\"display:block;white-space:normal;overflow:auto;max-width:200px;\">" +
        $("#"+td_description_id+counter).attr("fulltext") +
        " <a href=\"#\" onClick=\"hideDescription('" + td_description_id + "'," + counter + ")\">[Collapse]</a>"
    );
}

function loadOntologyNode(ontology, IRI, td_label_id, td_description_id, counter) {
    var label = "<i>Label could not be found</i>";
    var description = "<i>Description could not be found</i>";

    $.getJSON(
        "http://www.ebi.ac.uk/ols/beta/api/ontologies/" + ontology.toUpperCase() + "/terms?obo_id=" + ontology.toUpperCase() + ":" + IRI,
       function(ontology_json) {
            if (validOntologyNode(ontology_json)) {
                label = ontology_json._embedded.terms[0].label;
                description = ontology_json._embedded.terms[0].description[0];
            }
            $("#"+td_label_id+counter).html("<span style=\"display:block;max-width:100px;overflow:auto;white-space:normal;\">" + label + "</span>");
            $("#"+td_description_id+counter).html(
                "<a href=\"#\" style=\"cursor:hand\" onClick=\"showDescription('" + td_description_id + "'," + counter + ")\"><u>" +
                description.substring(0, 30) +
                (description.length >= 30 ? " [...]" : "") + 
                "</u></a>");
            $("#"+td_description_id+"_full"+counter).html(description);
            $("#"+td_description_id+counter).attr("fulltext", description);
        }
    );
}
