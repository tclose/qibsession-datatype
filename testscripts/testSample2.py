import glob
import fnmatch
import hashlib 
import inspect
import os
import pyxnat
import shlex
import shutil
import subprocess
import sys

def uploadImages(directory, project, subject, session):
  print "Uploading " + directory + "."

  # subject
  theSubject = project.subject(subject)
  if (not theSubject.exists()):
    theSubject.create()    

  # session      
  theSession = theSubject.experiment(session)
  if (not theSession.exists()):
    theSession.create()

  # scans
  for scanID in range (1, 61):
    filelist = glob.glob(directory+"/%06d*" % scanID)
    for filepath in filelist:
      filename = os.path.basename(filepath)
      theSession.scan('TEST'+str(scanID)).resource('IMA').file(filename).insert(filepath, 'IMA')
        
  return

def downloadImages(xnat, downloadDir, project, subject, session):
  print "Downloading: project="+project+", subject="+subject+", session="+session
  allscans = xnat.select.project(project).subject(subject).experiment(session).scans()
  for scan in allscans:
    allscans.download(downloadDir, type='ALL', extract=True)
    return

def md5ForFile(f, block_size):
  md5 = hashlib.md5()
  while True:
    data = f.read(block_size)
    if not data:
      break
    md5.update(data)
  return md5.hexdigest()

def fileCount(directory):
  total = 0
  for dirs, roots, files in os.walk(directory):
    total += len(files)
  return total

def md5NumberForDirectory(directory):
  sum = 0
  for dirs, roots, filenames in os.walk(directory):
    for filename in fnmatch.filter(filenames, '*'):
      md5sum = md5ForFile(open(os.path.join(dirs,filename)), 128) # Python 2.5> converts automatically from int to long
      sum += int(md5sum, 16)
  return sum

def checkImages(uploadDir, downloadDir):
  print "Comparing: originalDir={0}, downloadDir={1}".format(uploadDir, downloadDir)

  # First compare file count
  uploadCount = fileCount(uploadDir)
  downloadCount = fileCount(downloadDir)

  if uploadCount != downloadCount: 
    raise NameError("Test failed! Reason: file counts upload and download directories not equal.")

  # Then compare md5 sums
  uploadMD5 = md5NumberForDirectory(uploadDir)
  downloadMD5 = md5NumberForDirectory(downloadDir)
  
  if uploadMD5 != downloadMD5:
    raise NameError("Test failed! Reason: MD5's of directory not equal.")

  print "Test successful!"

  return

def run():
  xnat = pyxnat.Interface("URL", user="USER", password="PASSWORD",cachedir="CACHEDIR")
  projectName = "NightlyTest" 
  subjectName = "testSubjectSample2"
  sessionName =  "testSessionSample2"
  downloadDirRoot = "MAIN_DOWNLOAD_DIRECTORY"
  downloadDir = "DOWNLOAD_DIRECTORY_OF_SESSION"
  uploadDir = "UPLOAD_DIRECTORY"
  
  # Run the test
  project = xnat.select.project(projectName)
  uploadImages(uploadDir, project, subjectName, sessionName) # set auto-archive enabled online
  downloadImages(xnat, downloadDirRoot, projectName, subjectName, sessionName)
  checkImages(uploadDir, downloadDir)

  # Cleanup
  zipFile = downloadDirRoot+projectName+"_"+subjectName+"_"+sessionName+"_scans_ALL.zip"
  if (os.path.exists(zipFile)):
    os.remove(zipFile)

  if (os.path.exists(downloadDir)):
    shutil.rmtree(downloadDir)

  project.subject(subjectName).delete()

# Entry point
run()
